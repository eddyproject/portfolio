import React, {useState} from 'react'
import {Link} from "react-router-dom";
import {logo} from "../../assets";
import {useTranslation} from "react-i18next";
import MobileNavLinks from "./MobileNavLinks.jsx";
import NavLinks from "./NavLinks.jsx";
import {styles} from "../../styles.js";


const Navbar = () => {
    const {t, i18n} = useTranslation();
    const [active, setActive] = useState('')
    const [toggle, setToggle] = useState(false)

    const changeLanguage = (lng) => {
        i18n.changeLanguage(lng.target.value);
    }

    return (
        <nav
            className={`${styles.paddingX} w-full flex items-center py-5 fixed top-0 z-20 bg-primary`}>
            <div className="w-full flex justify-between items-center max-w-7xl mx-auto">
                <Link to='/' className="flex items-center gap-2" onClick={() => {
                    setActive('');
                    window.scrollTo(0, 0)
                }}>
                    <img src={logo} alt="logo" className="w-9 h-9 object-contain"/>
                    <p className='font-bold text-white text-[18px] cursor-pointer flex'>Eddy &nbsp;<span
                        className='sm:block hidden'> | &nbsp; Makboul</span></p>

                </Link>
                <NavLinks active={active} setActive={setActive} language={i18n.language}
                          changeLanguage={changeLanguage} navLinks={t(`headers.navlinks`,{ returnObjects: true } )}/>
                <MobileNavLinks changeLanguage={changeLanguage} active={active} setActive={setActive}
                                language={i18n.language} setToggle={setToggle} toggle={toggle} navLinks={t(`headers.navlinks`,{ returnObjects: true } )}/>
            </div>

        </nav>
    )
}

export default Navbar
import React from "react";
import {france, unitedKingdom} from '../../assets';

const languageList = [
    {value: "fr", label: "FR", icon: france},
    {value: "en", label: "EN", icon: unitedKingdom}
];
const LanguageSelector = ({currentLanguage, onLanguageChange}) => (
    <select onChange={onLanguageChange} defaultValue={currentLanguage}>
        {languageList.map(({value, label, icon}) => (
            <option key={value} value={value}>
                {label}
            </option>
        ))}
    </select>

);


export default LanguageSelector;
import React from "react";
import LanguageSelector from "./LanguageSelector.jsx";
import {close, menu} from "../../assets";

const MobileNavLinks = ({active, setActive, language, changeLanguage, setToggle, toggle, navLinks}) => (
    <div className="sm:hidden flex flex-1 justify-end items-center">
        <img src={toggle ? close : menu} alt="menu"
             className="w-[28px] h-[28px] cursor-pointer" onClick={() => setToggle(!toggle)}/>
        <div
            className={`${!toggle ? 'hidden' : 'flex'} p-6 black-gradient absolute top-20 right-0 mx-4 my-2 min-w-[140px] z-10 rounded-xl`}>

            <ul className="list-none flex justify-end items-start flex-col gap-4">
                {navLinks.map((link) => (
                    <li key={link.id}
                        className={`${active === link.id
                            ? "text-white"
                            : "text-secondary"} 
                                        font-poppins font-medium cursor-pointer text-[16px]`}
                        onClick={() => {
                            setActive(link.id);
                            setToggle(!toggle);
                        }}>
                        <a href={`#${link.id}`}>{link.text}</a>
                    </li>
                ))}
                <LanguageSelector currentLanguage={language} onLanguageChange={changeLanguage}/>
            </ul>
        </div>
    </div>
);

export default MobileNavLinks;
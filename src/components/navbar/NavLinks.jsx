
import LanguageSelector from "./LanguageSelector.jsx";
import React from "react";

const NavLinks = ({active, setActive, language, changeLanguage,navLinks}) => (
    <ul className="list-none hidden sm:flex flex-row gap-10">
        {navLinks.map((link) => (
            <li key={link.id}
                className={`${active === link.id
                    ? "text-white"
                    : "text-secondary"} 
                                hover:text-white text-[18px] font-medium cursor-pointer`}
                onClick={() => setActive(link.id)}>
                <a href={`#${link.id}`}>{link.text}</a>
            </li>
        ))}
        <LanguageSelector currentLanguage={language} onLanguageChange={changeLanguage}/>
    </ul>
);

export default NavLinks;
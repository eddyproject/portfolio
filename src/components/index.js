import { EarthCanvas, BallCanvas, ComputersCanvas, StarsCanvas } from './canvas';
import Hero from "./Hero";
import Navbar from "./navbar/Navbar"
import About from "./About/About.jsx";
import Tech from "./Tech";
import Experience from "./Experience";
import Works from "./Works";
import Contact from "./Contact";
import CanvasLoader from "./Loader";


export {
  Hero,
  Navbar,
  About,
  Tech,
  Experience,
  Works,
  Contact,
  CanvasLoader,
  EarthCanvas,
  BallCanvas,
  ComputersCanvas,
  StarsCanvas
};
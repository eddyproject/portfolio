import React from "react";
import {VerticalTimeline, VerticalTimelineElement,} from "react-vertical-timeline-component";
import {motion} from "framer-motion";

import "react-vertical-timeline-component/style.min.css";

import {styles} from "../styles";
import {SectionWrapper} from "../hoc";
import {textVariant} from "../utils/motion";
import {useTranslation} from "react-i18next";
import {stringToIcon} from "../utils/iconMapper.js";


const ExperienceCard = ({experience}) => {

    return (
        <VerticalTimelineElement
            contentStyle={{
                background: "#1d1836",
                color: "#fff",
            }}
            contentArrowStyle={{borderRight: "7px solid  #232631"}}

            date={experience.date}
            iconStyle={{background: experience.iconBg}}
            icon={
                <div className='flex justify-center items-center w-full h-full'>
                    <img src={stringToIcon(experience.icon)}
                         alt={experience.company}
                         className='w-[60%] h-[60%]object-contain'/>
                </div>
            }
        >
            <div>
                <h3 className="text-white font-bold text-[24px]">{experience.title}</h3>
                <p
                    className='text-secondary text-[16px] font-semibold'
                    style={{margin: 0}}
                >
                    {experience.company}
                </p>
                <ul className='mt-5 list-disc ml-5 space-y-2'>
                    {experience.points.map((point) => (
                        <li
                            key={`experience-point-${point}`}
                            className='text-white-100 text-[14px] pl-1 tracking-wider'
                        >
                            {point}
                        </li>
                    ))}
                </ul>
            </div>
        </VerticalTimelineElement>
    );
}
const Experience = () => {
    const [t] = useTranslation();
    return (
        <>
            <motion.div variants={textVariant()}>
                <p className={`${styles.sectionSubText} text-center`}>
                    {t('experience.subtitle')}
                </p>
                <h2 className={`${styles.sectionHeadText} text-center`}>
                    {t('experience.title')}
                </h2>
            </motion.div>

            <div className='mt-20 flex flex-col'>
                <VerticalTimeline>
                    {t('experience.jobs', {returnObjects: true})
                        .map((experience, index) => (
                        <ExperienceCard key={`experience-${index}`}
                                        experience={experience}/>
                        ))}
                    {/*{experiences.map((experience, index) => (*/}
                    {/*    <ExperienceCard key={`experience-${index}`}*/}
                    {/*                    experience={t(`experience.jobs.${experience.id}`, {returnObjects: true})}/>*/}
                    {/*))}*/}
                </VerticalTimeline>
            </div>
        </>
    )
}

export default SectionWrapper(Experience, "Experience");
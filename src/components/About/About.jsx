import React from "react";
import {motion} from "framer-motion";

import {styles} from "../../styles.js";
import {fadeIn, textVariant} from "../../utils/motion.js";
import SectionWrapper from "../../hoc/SectionWrapper.jsx";
import {useTranslation} from "react-i18next";
import {Map} from "./Map.jsx";

// const ServiceCard = ({index, title, icon}) => (
//     <ReactParallaxTilt className='xs:w-[250px] w-full'>
//         <motion.div
//             variants={fadeIn("right", "spring", index * 0.5, 0.75)}
//             className='w-full green-pink-gradient p-[1px] rounded-[20px] shadow-card'
//         >
//             <div
//                 options={{
//                     max: 45,
//                     scale: 1,
//                     speed: 450,
//                 }}
//                 className='bg-tertiary rounded-[20px] py-5 px-12 min-h-[280px] flex justify-evenly items-center flex-col'
//             >
//                 <img
//                     src={stringToIcon(icon)}
//                     alt='web-development'
//                     className='w-16 h-16 object-contain'
//                 />
//
//                 <h3 className='text-white text-[20px] font-bold text-center'>
//                     {title}
//                 </h3>
//             </div>
//         </motion.div>
//     </ReactParallaxTilt>
// );

const About = () => {
    const [t] = useTranslation();
    return (
        <div className="w-100 h-100 flex justify-between gap-50">
            <div className="flex flex-1 items-center justify-center sm:justify-end">
                <motion.div>
                    <motion.div variants={textVariant()}>
                        <p className={styles.sectionSubText}>{t('about.subtitle')}</p>
                        <h2 className={styles.sectionHeadText}>{t('about.title')}</h2>
                    </motion.div>

                    <motion.p
                        variants={fadeIn("", "", 0.1, 1)}
                        className='mt-4 text-secondary text-[17px] max-w-3xl leading-[30px]'
                    >
                        {t('about.description')}
                    </motion.p>
                </motion.div>
            </div>
            <div className="hidden sm:flex sm:flex-1 ">
                <Map/>
            </div>
        </div>
    );
};

export default SectionWrapper(About, "about");
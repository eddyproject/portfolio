import {Annotation, ComposableMap, Geographies, Geography} from "react-simple-maps";

export const Map = () => {
    return (
        <ComposableMap
            projection="geoAzimuthalEqualArea"
            projectionConfig={{
                rotate: [-10.0, -52.0, 0],
                center: [-5, -3],
                scale: 2000
            }}
            style={{width: "100%", height: "100%"}}
        >
            <Geographies
                geography="/features.json"
                fill="#2C065D"
                stroke="#FFFFFF"
                strokeWidth={0.5}
            >
                {({geographies}) =>
                    geographies.map((geo) => (
                        <Geography key={geo.rsmKey} geography={geo} style={{
                            default: {outline: "none"},
                            hover: {outline: "none"},
                            pressed: {outline: "none"},
                        }}/>
                    ))
                }
            </Geographies>
            <Annotation
                subject={[5.4228090259039154, 43.50927295107164]}
                dx={-90}
                dy={-30}
                connectorProps={{
                    stroke: "white",
                    strokeWidth: 2,
                    strokeLinecap: "round"
                }}
            >
                <text x="-8" textAnchor="end" alignmentBaseline="middle" fill="white">
                    {"Aix-en-Provence"}
                </text>
            </Annotation>
        </ComposableMap>
    );
};
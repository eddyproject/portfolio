import i18n from "i18next";
import {initReactI18next} from "react-i18next";

import enTranslation from './locales/en/translation.json';
import frTranslation from './locales/fr/translation.json';
import I18NextHttpBackend from "i18next-http-backend";
import I18nextBrowserLanguageDetector from "i18next-browser-languagedetector";

i18n
    .use(I18NextHttpBackend)
    .use(I18nextBrowserLanguageDetector)
    .use(initReactI18next)
    .init({
        resources: {
            en: {
                translation: enTranslation
            },
            fr: {
                translation: frTranslation
            }
        },
        lng: "fr",
        fallbackLng: "fr",
        interpolation: {
            escapeValue: false
        }
    });

export default i18n;
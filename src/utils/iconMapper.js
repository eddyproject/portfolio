import {
    capgemini,
    carrent,
    creator,
    css,
    docker,
    figma,
    git,
    html,
    javascript,
    jobit,
    meta,
    mobile,
    mongodb,
    nodejs,
    reactjs,
    redux,
    shopify,
    soprasteria,
    starbucks,
    tailwind,
    tesla,
    threejs,
    tripguide,
    typescript,
    web,
} from '../assets';


export const stringToIcon = (icon) => {
    switch (icon) {
        case "starbucks":
            return starbucks;
        case "carrent":
            return carrent;
        case "creator":
            return creator;
        case "css":
            return css;
        case "docker":
            return docker;
        case "figma":
            return figma;
        case "git":
            return git;
        case "html":
            return html;
        case "javascript":
            return javascript;
        case "jobit":
            return jobit;
        case "meta":
            return meta;
        case "mobile":
            return mobile;
        case "mongodb":
            return mongodb;
        case "nodejs":
            return nodejs;
        case "reactjs":
            return reactjs;
        case "redux":
            return redux;
        case "shopify":
            return shopify;
        case "tailwind":
            return tailwind;
        case "tesla":
            return tesla;
        case "threejs":
            return threejs;
        case "tripguide":
            return tripguide;
        case "typescript":
            return typescript;
        case "web":
            return web;
        case "soprasteria":
            return soprasteria;
        case "capgemini":
            return capgemini;
        default:
            return starbucks;
    }
}